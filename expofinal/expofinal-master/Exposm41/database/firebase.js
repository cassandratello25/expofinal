import firebase from "firebase";
import "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyAo6UG8biXy29eZMaYsjS41oK7J7hyXxyY",
  authDomain: "sm41-bdd55.firebaseapp.com",
  databaseURL: "https://sm41-bdd55.firebaseio.com",
  projectId: "sm41-bdd55",
  storageBucket: "sm41-bdd55.appspot.com",
  messagingSenderId: "196449911411",
  appId: "1:196449911411:web:f237c24c1ccc1f6da5e006"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();

export default {
  firebase,
  db
};
